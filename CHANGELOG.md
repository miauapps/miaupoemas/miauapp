- 1812.1-RC
  - Creacion de maqueta de vistas simples y funcionales

- 1812.2-RC
  - Se corrige error al mostrar regalitos
  - Aplicacion se conecta al web service correctamente
  - Se agrega icono a la aplicación

- 1812.3-RC
  - Se agrega funcion de comprobacion de nueva versión por medio
    de una notificacion
  - Se embellece grafica de carga de regalos

- 1812.4-RC
  - Se agrega comprobador de conexion a internet

- 1812.4.1-RC
  - Se cambia interfaz del actualizador y se modifican algunos detalles de codigo

- 1812.4.2-RC
  - Se modifican detalles graficos y se corrigen errores al descargar cuando
    no existe conexión a internet.

- 1812.5-RC
  - Se corrige error con android 6 al momento de descargar actualizacion

- 1812.5.1-RC
  - Se corrige error al mostrar fecha, el dia no se mostraba correctamente

- 1812.6-RC
  - Se modifica session
  - Se agrega funcion para revisar si existen regalos nuevos
  - Se corrigen errores menores

- 1812.7-RC
  - La funcion versionchecker ahora es un servicio

- 1812.8-RC
  - Los servicios de la aplicacion se ejecutan al iniciar el dispositivo

- 1812.9-RC
  - Se modifica ventana de descarga, ahora se utiliza un progressBar horizontal
  - Se modifica el menu, ahora se muestra en check cuando el regalo ya se vio
  - Se crea splashscreen y ahora el login es automatico si el texto coincide con la contraseña

- 1812.10-RC
  - Se implementa compatibilidad con ultimas versiones de android para el caso de las notificaciones y el receiver
  - Se corrige error con receiver, ahora debe iniciar la app sin problemas al inicio, compatible con versiones posteriores de android

- 1905.1-RC
  - Migracion a androidx

- 1905.2-Beta
  - Migracion a Flutter
  - Se agregan funciones basicas
  - Falta agregar Services, Boot Receive y Notificaciones.

- 1905-3-Beta
  - Se adapta app a nueva version de web service
  - Correcciones y mejoras menores
  - Se sube app a google play

- 1906.1-Beta
  - Soporte de notificaciones agregado y testeado pero no implementado
  - Se agrega validacion de leido y se setea el readed cuando se lee regalo
  - Se agrega opcion de refrescar menu
  - Cuando se vuelve a menu se actualiza para verificar cambios

- 1906.1.1-RC
  - Se corrige error de compilacion

