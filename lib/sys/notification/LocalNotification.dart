import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class LocalNotification {
  final int id;
  final String icon;
  final String title;
  final String body;
  final String payload;
  final SelectNotificationCallback onSelect;

  LocalNotification({
    this.id,
    this.icon,
    this.title,
    this.body,
    this.payload,
    this.onSelect,
  });
}
