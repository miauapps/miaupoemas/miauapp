import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'LocalNotification.dart';

class NotificationManager {
  FlutterLocalNotificationsPlugin _notifPlugin;

  static final NotificationManager _instance = NotificationManager();

  static NotificationManager getInstance() => _instance;

  NotificationManager() {
    _notifPlugin = FlutterLocalNotificationsPlugin();
  }

  Future _configurePlugin(LocalNotification notification) async {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings(notification.icon);
    /*var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: (){};*/
    var initializationSettings =
        new InitializationSettings(initializationSettingsAndroid, null);
    _notifPlugin.initialize(initializationSettings,
        onSelectNotification: notification.onSelect);
  }

  Future sendNotification(LocalNotification notification) async {
// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    _configurePlugin(notification);
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await _notifPlugin.show(
      notification.id,
      notification.title,
      notification.body,
      platformChannelSpecifics,
      payload: notification.payload,
    );

    Future cancelNotification(int id) async {
      await _notifPlugin.cancel(id);
    }
  }
}
