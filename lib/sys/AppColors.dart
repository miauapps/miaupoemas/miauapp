import 'dart:ui';

class AppColors {
  static final Color PRIMARY_COLOR = Color(0xff008577);
  static final Color ACCENT_COLOR = Color(0xff1bd8ac);
  static final Color SPLASH_COLOR = Color(0xffffffff);
}
