class PresentType {
  int id;
  String name;

  factory PresentType.fromJson(Map<String, dynamic> json) {
    return PresentType(id: json["id"], name: json["name"]);
  }

  PresentType({int id, String name}) {
    this.id = id;
    this.name = name;
  }
}
