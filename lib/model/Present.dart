import 'dart:convert';
import 'dart:typed_data';

import 'package:miau_app/model/PresentType.dart';

class Present {
  int id;
  PresentType type;
  Uint8List icon;
  DateTime time;
  bool readed;

  factory Present.fromJson(Map<String, dynamic> json) {
    return Present(
        icon: json["icon"],
        id: json["id"],
        type: PresentType.fromJson(json["type"]),
        readed: json["readed"],
        time: json["time"]);
  }

  Present(
      {int id, PresentType type, Uint8List icon, DateTime time, bool readed}) {
    this.id = id;
    this.type = type;
    this.icon = icon;
    this.time = time;
    this.readed = readed;
  }
}
