import 'dart:typed_data';

class ViewPresentMenu {
  int id;
  Uint8List icon;
  String title;
  DateTime time;
  bool readed;

  factory ViewPresentMenu.fromJson(Map<String, dynamic> json) {
    return ViewPresentMenu(
        icon: json["icon"],
        id: json["id"],
        readed: json["readed"],
        time: json["time"],
        title: json["title"]);
  }

  ViewPresentMenu(
      {int id, Uint8List icon, String title, DateTime time, bool readed}) {
    this.id = id;
    this.icon = icon;
    this.title = title;
    this.time = time;
    this.readed = readed;
  }
}
