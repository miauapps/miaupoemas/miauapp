import 'dart:convert';

import 'Present.dart';

class Poem {
  int id;
  Present present;
  String title;
  String body;

  factory Poem.fromJson(Map<String, dynamic> json) {
    return Poem(
        body: json["body"],
        id: json["id"],
        present: Present.fromJson(json["present"]),
        title: json["title"]);
  }

  Poem({int id, Present present, String title, String body}) {
    this.id = id;
    this.present = present;
    this.title = title;
    this.body = body;
  }
}
