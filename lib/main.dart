import 'package:flutter/material.dart';
import 'package:miau_app/sys/AppColors.dart';

import 'ui/LoginWidget.dart';
import 'ui/MenuWidget.dart';
import 'ui/PresentWidget.dart';

void main() => runApp(MiauApp());

class MiauApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  State<StatefulWidget> createState() => MiauAppState();
}

class MiauAppState extends State<MiauApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Miau App',
      theme: ThemeData(
          primaryColor: AppColors.PRIMARY_COLOR,
          accentColor: AppColors.ACCENT_COLOR),
      home: LoginWidget(),
      routes: {
        '/menu': (context) => MenuWidget(),
        '/present': (context) => PresentWidget(),
      },
    );
  }
}
