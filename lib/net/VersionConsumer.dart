import 'package:miau_app/sys/AppInfo.dart';

import 'Consumer.dart';

class VersionConsumer extends Consumer {
  VersionConsumer({String host, int port, String servicePath})
      : super(
            host: host == null ? AppInfo.SERVER_HOST : host,
            port: port == null ? AppInfo.SERVER_PORT : port,
            servicePath: "/version");

  Future<bool> isLast(String currentVersion) async {
    return (await consumeString("/islast", [currentVersion])).toLowerCase() ==
        true;
  }
}
