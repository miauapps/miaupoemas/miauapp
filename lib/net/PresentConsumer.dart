import 'package:miau_app/model/Present.dart';
import 'package:miau_app/model/ViewPresentMenu.dart';
import 'package:miau_app/net/Consumer.dart';
import 'package:miau_app/sys/AppInfo.dart';

class PresentConsumer extends Consumer {
  PresentConsumer({String host, int port, String servicePath})
      : super(
            host: host == null ? AppInfo.SERVER_HOST : host,
            port: port == null ? AppInfo.SERVER_PORT : port,
            servicePath: "/present");

  Future<bool> hasNewPresent() async {
    return (await consumeString("/hasnew", [])).toLowerCase() == true;
  }

  Future<List<Present>> getAllPoems() async {
    return (await consumeList("/getall", []))
        .map((poem) => Present.fromJson(poem))
        .toList();
  }

  Future<List<ViewPresentMenu>> getAllViews() async {
    return (await consumeList("/getallviews", []))
        .map((view) => ViewPresentMenu.fromJson(view))
        .toList();
  }

  Future<Present> getPoem(int id) async {
    return Present.fromJson(await consume("", [id.toString()]));
  }

  void setRead(int presentId) {
    patch("/read", [presentId.toString()]);
  }
}
