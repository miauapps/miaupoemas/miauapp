import 'package:miau_app/model/Present.dart';
import 'package:miau_app/model/ViewPresentMenu.dart';
import 'package:miau_app/net/Consumer.dart';
import 'package:miau_app/sys/AppInfo.dart';

class ViewConsumer extends Consumer {
  ViewConsumer({String host, int port, String servicePath})
      : super(
            host: host == null ? AppInfo.SERVER_HOST : host,
            port: port == null ? AppInfo.SERVER_PORT : port,
            servicePath: "/viewmenu");

  Future<List<ViewPresentMenu>> getAll() async {
    return (await consumeList("", []))
        .map((view) => ViewPresentMenu.fromJson(view))
        .toList();
  }
}
