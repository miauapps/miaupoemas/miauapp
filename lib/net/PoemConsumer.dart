import 'package:miau_app/model/Poem.dart';
import 'package:miau_app/net/Consumer.dart';
import 'package:miau_app/sys/AppInfo.dart';

class PoemConsumer extends Consumer {
  PoemConsumer({String host, int port, String servicePath})
      : super(
            host: host == null ? AppInfo.SERVER_HOST : host,
            port: port == null ? AppInfo.SERVER_PORT : port,
            servicePath: "/poem");

  Future<List<Poem>> getAllPoems() async {
    return (await consumeList("/", []))
        .map((poem) => Poem.fromJson(poem))
        .toList();
  }

  // cuando hay parametros no se coloca slash
  Future<Poem> getPoemOf(int idPresent) async {
    return Poem.fromJson(await consume("", [idPresent.toString()]));
  }
}
