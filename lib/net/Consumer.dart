import 'dart:convert';
import 'package:miau_app/sys/AppInfo.dart';
import 'URLConnector.dart';

class Consumer {
  URLConnector connector;

  String _servicePath;

  Consumer({String host, int port, String servicePath}) {
    connector = URLConnector(
        host: host, port: port == null ? AppInfo.SERVER_PORT : port);
    _servicePath = servicePath;
  }

  String formatPath(String urlPath) {
    return _servicePath + urlPath;
  }

  void post(String urlPath, List<String> parameters) async {
    await connector.post(formatPath(urlPath), parameters);
  }

  void patch(String urlPath, List<String> parameters) async {
    await connector.patch(formatPath(urlPath), parameters);
  }

  Future<Map<String, dynamic>> consume(
      String urlPath, List<String> parameters) async {
    // return then es lo mismo que return con await solo que en este ultimo
    // se convierte la funcion en asincrona
    String urlContent =
        await connector.getUrlContent(formatPath(urlPath), parameters);
    return jsonDecode(urlContent);
  }

  Future<List<dynamic>> consumeList(
      String urlPath, List<String> parameters) async {
    String urlContent =
        await connector.getUrlContent(formatPath(urlPath), parameters);
    return jsonDecode(urlContent);
  }

  Future<String> consumeString(String urlPath, List<String> parameters) async {
    return await connector.getUrlContent(formatPath(urlPath), parameters);
  }

  void consumeNothing(String urlPath, List<String> parameters) async {
    await connector.getUrlContent(formatPath(urlPath), parameters);
  }

  // con await se juntan los bloques de futures en uno solo
  /*Future<PlayerResponse> consumeResponse(String urlPath, List<String> parameters) async {
    return PlayerResponse.fromJson(await consume(urlPath, parameters));
  }*/

}
