import 'dart:ui';

import 'package:flutter/material.dart';

class Blurred extends StatelessWidget {
  final Widget widget;
  final double sigmaX;
  final double sigmaY;

  const Blurred({Key key, this.widget, this.sigmaX, this.sigmaY})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double x = sigmaX == null ? 8 : sigmaX;
    double y = sigmaY == null ? 8 : sigmaY;

    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        widget,
        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: x, sigmaY: y),
          child: Container(
            color: Colors.transparent,
          ),
        ),
      ],
    );
  }
}
