import 'package:flutter/material.dart';
import 'package:miau_app/ui/BlurImage.dart';
import 'package:miau_app/util/DisplayUtil.dart';

class Backgrounded extends StatelessWidget {
  final Positioned child;
  final String imgSrc;
  final bool blurred;

  const Backgrounded({Key key, this.child, this.imgSrc, this.blurred})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Positioned(
          child: blurred
              ? BlurImage(imgSrc: imgSrc)
              : Image(
                  image: AssetImage(imgSrc),
                  fit: BoxFit.fill,
                ),
        ),
        child
      ],
    );
  }
}
