import 'package:flutter/material.dart';
import 'package:miau_app/model/ViewPresentMenu.dart';
import 'package:miau_app/net/ViewConsumer.dart';
import 'package:miau_app/sys/AppColors.dart';
import 'package:miau_app/ui/MenuCard.dart';
import 'package:miau_app/util/DisplayUtil.dart';

class MenuWidget extends StatefulWidget {
  MenuWidget({Key key}) : super(key: key);

  _MenuWidgetState createState() => _MenuWidgetState();
}

class _MenuWidgetState extends State<MenuWidget> {
  ViewConsumer _consumer;
  Future<List<ViewPresentMenu>> _listPresents;

  _MenuWidgetState() {
    _consumer = ViewConsumer();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _listPresents = _consumer.getAll();
    });
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.PRIMARY_COLOR,
          title: Text("Listado de Poemas"),
        ),
        body: Container(
          child: SizedBox(
            height: double.infinity,
            width: DisplayUtil.getWidth(context, perc: 95),
            child: RefreshIndicator(
              onRefresh: _refresh,
              child: FutureBuilder(
                future: _listPresents,
                builder: (ctx, snapshot) {
                  if (snapshot.hasData) {
                    return _getPresentList(snapshot.data);
                  } else if (snapshot.hasError) {
                    print("Error: ${snapshot.error}");
                    return _getErrorWidget("Error: ${snapshot.error}");
                  } else {
                    return _getLoadingWidget();
                  }
                },
              ),
            ),
          ),
        ));
  }

  ListView _getPresentList(List<ViewPresentMenu> listPresents) {
    return ListView.builder(
      itemCount: listPresents.length,
      itemBuilder: (ctx, pos) {
        return MenuCardWidget(present: listPresents[pos]);
      },
    );
  }

  Widget _getErrorWidget(String msg) {
    return Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(top: 10),
      width: DisplayUtil.getWidth(context, perc: 30),
      height: DisplayUtil.getHeight(context, perc: 20),
      child: Column(
        children: <Widget>[
          Text(
            msg,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
          ),
          Image.asset("assets/img/ic_launcher.png")
        ],
      ),
    );
  }

  Widget _getLoadingWidget() {
    return Container(
      alignment: Alignment.topCenter,
      margin: EdgeInsets.only(top: 10),
      width: DisplayUtil.getWidth(context, perc: 30),
      height: DisplayUtil.getHeight(context, perc: 20),
      child: CircularProgressIndicator(),
    );
  }

  Future<void> _refresh() async {
    setState(() {
      _listPresents = _consumer.getAll();
    });
  }
}
