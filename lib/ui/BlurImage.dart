import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:ui';

import 'package:miau_app/ui/Blurred.dart';

class BlurImage extends StatelessWidget {
  var _imgSrc;

  BlurImage({var imgSrc}) {
    this._imgSrc = imgSrc;
  }

  @override
  Widget build(BuildContext context) {
    ImageProvider img = _imgSrc.runtimeType.toString().contains("String")
        ? AssetImage(_imgSrc.toString())
        : MemoryImage(_imgSrc as Uint8List);

    return Blurred(
      sigmaX: 8,
      sigmaY: 8,
      widget: Image(
        image: img,
        fit: BoxFit.fill,
      ),
    );
  }
}
