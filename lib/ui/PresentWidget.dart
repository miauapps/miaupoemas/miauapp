import 'package:flutter/material.dart';
import 'package:miau_app/model/Poem.dart';
import 'package:miau_app/net/PoemConsumer.dart';
import 'package:miau_app/ui/Backgrounded.dart';
import 'package:miau_app/util/DisplayUtil.dart';

import 'SeparatorWidget.dart';

class PresentWidget extends StatefulWidget {
  final int presentId;

  const PresentWidget({this.presentId});

  _PresentWidgetState createState() => _PresentWidgetState(presentId);
}

class _PresentWidgetState extends State<PresentWidget> {
  final int presentId;

  _PresentWidgetState(this.presentId);

  Widget _getLoadingWidget() {
    return Align(
      alignment: Alignment.center,
      child: SizedBox(
        width: DisplayUtil.getWidth(context, perc: 20),
        height: DisplayUtil.getHeight(context, perc: 10),
        child: CircularProgressIndicator(
          backgroundColor: Colors.blue,
          strokeWidth: 10,
        ),
      ),
    );
  }

  Widget _getLoadedInfo(Poem poem) {
    return Backgrounded(
      blurred: true,
      imgSrc: "assets/img/cat.png",
      child: Positioned(
        child: Column(
          children: <Widget>[
            Spacer(
              flex: 5,
            ),
            Flexible(
                flex: 10,
                fit: FlexFit.loose,
                child: Text(
                  poem.title,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                    fontWeight: FontWeight.w700,
                  ),
                )),
            Spacer(flex: 5),
            Flexible(
              flex: 80,
              fit: FlexFit.loose,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Text(
                  poem.body,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w500),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // ver en los widget de la semana el que sirve
    // para acomodar widgets rezise automatico

    /*
     * Widget
     * Widget
     * ESPACIO
     * ESPACION
     * Widget
     * 
    */

    PoemConsumer consumer = PoemConsumer();
    return Scaffold(
      body: FutureBuilder(
        future: consumer.getPoemOf(presentId),
        builder: (context, snapshot) {
          //return _getLoadingWidget();
          if (snapshot.hasData) {
            return _getLoadedInfo(snapshot.data);
          } else {
            return _getLoadingWidget();
          }
        },
      ),
    );
  }
}
