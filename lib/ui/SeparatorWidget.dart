import 'package:flutter/material.dart';

class SeparatorWidget extends StatelessWidget {
  final int flex;

  const SeparatorWidget({this.flex});

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: flex,
      fit: FlexFit.loose,
      child: Container(),
    );
  }
}
