import 'package:flutter/material.dart';
import 'package:miau_app/sys/AppInfo.dart';
import 'package:miau_app/ui/Backgrounded.dart';
import 'package:miau_app/util/DisplayUtil.dart';
//import 'package:simple_permissions/simple_permissions.dart';

class LoginWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginWidgetState();
}

class LoginWidgetState extends State<LoginWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        child: Backgrounded(
          blurred: true,
          imgSrc: "assets/img/wallpaper5.jpg",
          child: Positioned(
            top: DisplayUtil.getHeight(context, perc: 85),
            left: DisplayUtil.getWidth(context, perc: 18),
            width: DisplayUtil.getWidth(context, perc: 65),
            height: 40,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Container(
                color: Colors.white,
                child: TextField(
                    decoration: InputDecoration(
                      hintText: "Contraseña: ",
                    ),
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                    obscureText: true,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    keyboardAppearance: Brightness.light,
                    onChanged: (str) async {
                      if (str == AppInfo.PASSWORD) {
                        Navigator.of(context).pushNamed("/menu");
                      }
                    }),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
