import 'package:flutter/material.dart';
import 'package:miau_app/model/Poem.dart';
import 'package:miau_app/model/ViewPresentMenu.dart';
import 'package:miau_app/net/PoemConsumer.dart';
import 'package:miau_app/net/PresentConsumer.dart';
import 'package:miau_app/ui/PresentWidget.dart';

class MenuCardWidget extends StatelessWidget {
  ViewPresentMenu _present;

  MenuCardWidget({ViewPresentMenu present}) {
    this._present = present;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      borderOnForeground: true,
      margin: EdgeInsets.only(
        top: 3,
        bottom: 3,
        left: 10,
      ),
      child: InkWell(
        onTap: () async {
          PresentConsumer presentConsumer = PresentConsumer();
          presentConsumer.setRead(_present.id);
          
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PresentWidget(
                    presentId: _present.id,
                  ),
            ),
          );
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Flexible(
              flex: 20,
              fit: FlexFit.loose,
              child: Align(
                child: Image(
                  image: AssetImage("assets/img/list.png"),
                  width: 40,
                  height: 40,
                ),
              ),
            ),
            Flexible(
              flex: 60,
              fit: FlexFit.tight,
              child: Text(
                _present.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 14,
                ),
              ),
            ),
            Flexible(
              flex: 20,
              fit: FlexFit.loose,
              child: Align(
                child: _present.readed
                    ? Image(
                        image: AssetImage("assets/img/checked.png"),
                        width: 32,
                        height: 32,
                      )
                    : Container(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
