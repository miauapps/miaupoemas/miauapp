import 'package:flutter/material.dart';

// 80 -> bar height
class DisplayUtil {
  static Size _getDisplaySize(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Size(size.width, size.height - 80);
  }

  static double getWidth(BuildContext context, {double perc = 100}) {
    double width = _getDisplaySize(context).width;
    return width * (perc / 100);
  }

  static double getWidthFromParent(double parentWidth, {double perc = 100}) {
    return parentWidth * (perc / 100);
  }

  static double getHeight(BuildContext context, {double perc = 100}) {
    double height = _getDisplaySize(context).height;

    return height * (perc / 100);
  }

  static double getHeightFromParent(double parentHeight, {double perc = 100}) {
    return parentHeight * (perc / 100);
  }
}
